# Interface commands

This repo contains example functions of how commands may be parsed and responded to from an ESP32.

## Overview

Commands are received through a UDP socket, the contents are parse and, according to the command received an action is performed.

## Contents

respond_to_request: receives a command (```const char *request```), parses it, and calls the appropriate response function. Response functions are contained in responses.c


## Repository Structure

```
.
├── main.c
└── responses.c
```
