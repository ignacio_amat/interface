void response_time(void){

	/* Respond with the current local time for 'plidar time' */
	struct timeval tv;
	gettimeofday(&tv, NULL);
	struct tm timeinfo;
	localtime_r(&tv.tv_sec, &timeinfo);

	/* Format the time string with microseconds */
	char time_str[64];
	snprintf(time_str, sizeof(time_str), "local time %04d-%02d-%02d %02d:%02d:%02d.%06ld",
			timeinfo.tm_year + 1900, timeinfo.tm_mon + 1, timeinfo.tm_mday,
			timeinfo.tm_hour, timeinfo.tm_min, timeinfo.tm_sec,
			tv.tv_usec);

	snprintf(G_RESPONSE, BUFFER_SIZE, time_str);

}

void response_hello(void){

	snprintf(G_RESPONSE, BUFFER_SIZE, "{\"name\":\"AMAT\",\"connection_status\":\"Available\"}");

}

void response_password(void){

	snprintf(G_RESPONSE, BUFFER_SIZE, "OK");

}

#define RESPONSE_MASK(sock, source_addr, source_addr_len)\
	sendto(sock, gen_mask_json(MASK_SIZE), sizeof(result), 0, (struct sockaddr *)source_addr, source_addr_len);\
	return;

void response_setting(const char* request){

	const char* setting = request + strlen("setting:");
	snprintf(G_RESPONSE, BUFFER_SIZE, get_setting(setting, &config));

}

void response_information(const char* request){

	const char* information = request + strlen("information:");
	snprintf(G_RESPONSE, BUFFER_SIZE, get_information(information, G_IS_MEASURING));

}

void response_action(const char* request){

	const char* action = request + strlen("action:");
	snprintf(G_RESPONSE, BUFFER_SIZE, get_action(action, &config));

}

void response_mac(void){

	snprintf(G_RESPONSE, BUFFER_SIZE, G_MAC_STR );

}

void response_ip(void){

	snprintf(G_RESPONSE, BUFFER_SIZE, G_IP_STR );

}

void response_connection(void){
	/* Respond with the WiFi connection strength */
	wifi_ap_record_t wifi_info;
	esp_wifi_sta_get_ap_info(&wifi_info);
	snprintf(G_RESPONSE, BUFFER_SIZE, "WiFi strength: %d dBm", wifi_info.rssi);
}

void response_search(void){

	snprintf(G_RESPONSE, BUFFER_SIZE, "ESP32 found");

}

void response_sync(const char* request){
	/* Update local time based on 'plidar sync {unix_time}' message */
	char *unix_time_str = strstr(request, "plidar sync");
	if (unix_time_str) {
		long unix_time = atol(unix_time_str + strlen("plidar sync"));
		struct timeval tv;
		tv.tv_sec = unix_time;
		tv.tv_usec = 0;
		settimeofday(&tv, NULL);
		snprintf(G_RESPONSE, BUFFER_SIZE, "Local time reference set to [%ld]", unix_time);
	} else {
		snprintf(G_RESPONSE, BUFFER_SIZE, "Invalid 'plidar sync' message format");
	}

}

void plidar_reboot(int sock, struct sockaddr_storage *source_addr, socklen_t source_addr_len){

	snprintf(G_RESPONSE, BUFFER_SIZE, "Rebooting ESP32...");

	printf("Sent [%s] to [%s]\n", G_RESPONSE, inet_ntoa(((struct sockaddr_in *)source_addr)->sin_addr));

	int err = sendto(sock, G_RESPONSE, strlen(G_RESPONSE), 0, (struct sockaddr *)source_addr, source_addr_len);
	if (err < 0) {
		ESP_LOGE(TAG, "Error occurred during sending: errno %d", errno);
		return;
	}

	vTaskDelay(pdMS_TO_TICKS(1000)); // Add a delay to ensure G_RESPONSE is sent

	esp_restart(); // Reboot the ESP32

}

