#include <string.h>

#include "responses.c"

#define BUFFER_SIZE 1024
char G_RESPONSE[BUFFER_SIZE];

void respond_to_request(const char *request, int sock, struct sockaddr_storage *source_addr, socklen_t source_addr_len) {

	// Initialize date_time with Unix timestamp as a string
	time_t current_time;
	time(&current_time);
	snprintf(config.date_time, sizeof(config.date_time), "%ld", (long)current_time);

	snprintf(G_RESPONSE, BUFFER_SIZE, "Command Not Recognised.");
	/* Check the received request and prepare the appropriate G_RESPONSE */
	if (strstr(request, "plidar time"      )){response_time();}
	if (strstr(request, "hello"            )){response_hello();}
	if (strstr(request, "connect:password" )){response_password();}
	if (strstr(request, "setting:mask"     )){RESPONSE_MASK(sock, source_addr, source_addr_len);}
	if (strstr(request, "setting:"         )){response_setting(request);}
	if (strstr(request, "information:"     )){response_information(request);}
	if (strstr(request, "action:"          )){response_action(request);}
	if (strstr(request, "plidar mac"       )){response_mac();}
	if (strstr(request, "plidar ip"        )){response_ip();}
	if (strstr(request, "plidar connection")){response_connection();}
	if (strstr(request, "plidar search"    )){response_search();}
	if (strstr(request, "plidar sync"      )){response_sync(request);}
	if (strstr(request, "plidar reboot"    )){plidar_reboot(sock, source_addr, source_addr_len);}
	if (strstr(request, "plidar lenny"     )){snprintf(G_RESPONSE, BUFFER_SIZE, "( ͡° ͜ʖ ͡°)");}

	// Send the prepared G_RESPONSE to the requester
	int err = sendto(sock, G_RESPONSE, strlen(G_RESPONSE), 0, (struct sockaddr *)source_addr, source_addr_len);
	if (err < 0) {
		ESP_LOGE(TAG, "Error occurred during sending: errno %d", errno);
		return;
	} else {
		ESP_LOGI(TAG, "Sent response: %s", G_RESPONSE);
	}
}

int main(int argc, char *argv[]){

	/* Receive request from socket */

	/* Call respond_to_request with received request */

	return 0;
}
